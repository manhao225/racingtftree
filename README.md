# Racing TF Tree
华南理工大学方程式赛车队无人车使用的 TF 树
# 依赖
+ tf2
+ tf2_ros
+ fsd_common_msgs

# 构建
1. 将该仓库放在含有`fsd_common_msgs`的工作空间下
1. `catkin build`

# 使用

    roslaunch racingtftree run.launch

# 坐标系说明
所有坐标系全为右手系
## TF 树结构
![frames](frames.png)
## car
车辆坐标系，原点位于后轴中心
## cog
车辆质心坐标系。原点位于车辆质心，需要标定（已标定完成）
## imu
IMU 坐标系，原点位于 IMU 质心，需要标定
## rslidar
LiDAR 坐标系，原点位于 LiDAR 中心，需要标定（已标定完成）
## camera
相机坐标系，原点位于相机前面板中心，需要标定（已标定完成）