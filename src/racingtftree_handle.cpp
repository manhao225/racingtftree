#include "racingtftree_handle.h"

#include <iostream>

RacingTfTreeHandle::RacingTfTreeHandle() : node_("~") {

    if (!node_.param<std::string>("racingtftree/state_topic", state_topic_, "/estimation/slam/rear_axis_center_state")) {
        ROS_WARN_STREAM("Did not load racingtftree/state_topic. Standard value is: " << state_topic_);
    }
    if (!node_.param<std::string>("racingtftree/world_frame_id", world_frame_id_, "world")) {
        ROS_WARN_STREAM("Did not load racingtftree/world_frame_id. Standard value is: " << world_frame_id_);
    }
    if (!node_.param<std::string>("racingtftree/car_frame_id", car_frame_id_, "car")) {
        ROS_WARN_STREAM("Did not load racingtftree/car_frame_id. Standard value is: " << car_frame_id_);
    }

    // init static transforms
    loadPartParam("rslidar_to_car", transform_rslidar_car_);
    loadPartParam("cog_to_car", transform_cog_car_);
    loadPartParam("imu_to_car", transform_imu_car_);
    loadPartParam("camera_to_car", transform_camera_car_);

}



void RacingTfTreeHandle::start() {

    state_sub_ = node_.subscribe<fsd_common_msgs::CarState>(state_topic_, 1, &RacingTfTreeHandle::stateCallBack, this);

    std::cout << "TF Start! " << std::endl;

}

void RacingTfTreeHandle::stateCallBack(const fsd_common_msgs::CarState::ConstPtr& msg) {

    if(msg->header.frame_id != world_frame_id_) return;

    setTransformBasis(transform_car_world_, world_frame_id_, car_frame_id_);
    setTranslation(transform_car_world_, msg->car_state.x, msg->car_state.y, 0.0);
    tf2::Quaternion q;
    q.setRPY(0, 0, msg->car_state.theta);
    setRotation(transform_car_world_, q);

    br_car_world_.sendTransform(transform_car_world_);
    ROS_INFO("World --> car TF sended. ");

}



void RacingTfTreeHandle::sendStaticTransform() {

    br_rslidar_car_.sendTransform(transform_rslidar_car_);
    br_cog_car_.sendTransform(transform_cog_car_);
    br_rslidar_car_.sendTransform(transform_imu_car_);
    br_camera_car_.sendTransform(transform_camera_car_);

}



void RacingTfTreeHandle::loadPartParam(std::string part_name, geometry_msgs::TransformStamped &transform) {

    tf2::Quaternion q;
    q.setRPY(0, 0, 0);

    std::string frame_id;
    double x, y, z;

    if (!node_.param<std::string>("racingtftree/"+part_name+"/frame_id", frame_id, "rslidar")) {
        ROS_WARN("Did not load racingtftree/%s/frame_id. Standard value is: %s", part_name.c_str(), frame_id.c_str());
    }
    if (!node_.param<double>("racingtftree/"+part_name+"/x", x, 0)) {
        ROS_WARN("Did not load racingtftree/%s/frame_id. Standard value is: %f", part_name.c_str(), x);
    }
    if (!node_.param<double>("racingtftree/"+part_name+"/y", y, 0)) {
        ROS_WARN("Did not load racingtftree/%s/frame_id. Standard value is: %f", part_name.c_str(), y);
    }
    if (!node_.param<double>("racingtftree/"+part_name+"/z", z, 0)) {
        ROS_WARN("Did not load racingtftree/%s/frame_id. Standard value is: %f", part_name.c_str(), z);
    }
    setTransformBasis(transform, car_frame_id_, frame_id);
    setTranslation(transform, x, y, z);
    setRotation(transform, q);
}